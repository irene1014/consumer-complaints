## Insight Data Engineer Coding Challenge - Customer Complaints
---
## Table of Contents

1. Problem
2. Input datasets and Output 
3. Source
4. Test  

---
## Problem 

The challenge is provided by Insight for the candidates who are interested in Data Engineeer program. 
The goal of this challenge is about processing and analyzing the customer complaints data provided by the federal government. 
It identifies the number of complaints filed and the distribution of different companies regarding various financial issues. 

---
##  Input and Output 

The input file is **complaints.csv** which is saved in the input folder. 
I used 3 variables among 18 which are product, year, and company. 
After reading and processing it, I wrote the results to an output file, **report.csv** in output folder.
The output file inclueds 5 variables which are product, year, total number of complaints, total number of companies, and 
highest percentage of total complaints filed against one company for that product and year.

---
##  Source 

The Source is based on the programing language Python. 
The name of class is 'Insightcode' and initial parameters are 'inputfile' and 'outputfile'. 
The class is consisted of 2 instances which are process_dt and exportcsv. 

### 1. process_dt

This instance read and process the input file. I used python 'csv' module to read it and 
'Collections' module to group certain variables that are product and year. After grouping 2 variables, 
I counted the number of complaints and appended the variable 'company' in each group. Finally, 
I counted the number of companies, calculated the percentage of the complaints in each company and 
selected the highest number. I saved the result in a dictionary. 

### 2. exportcsv

This instance exports the dictionary that I made in 'process_dt' as a csv file and save in
output folder. 


The reason that I made two different instances is to check the processed result first and 
export the result later. 

---

## Test 

The script can be executed using this following code in Terminal.

```bash
python ./src/consumer_complaints.py ./input/complaints.csv ./output/report.csv
```

Two different unittest were executed using run.sh and compared the results using run_test.sh file. 
The input files were also 'complaints.csv' but they had different length with the origianl file.
The source file successfully passed the two unittest and gave 2 output files. 

